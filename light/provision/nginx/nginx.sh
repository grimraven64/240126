#!/bin/bash
echo -e "\033[43m Phase: ***** install Nginx ***** \033[0m"
sudo apt install nginx -y
sudo systemctl status nginx.service
sudo rm -rf /etc/nginx/sites-available/default
sudo cp -R /home/vagrant/provision/nginx/default /etc/nginx/sites-available/default
sudo nginx -t
sudo nginx -s reload