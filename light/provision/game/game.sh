#!/bin/bash
echo -e "\033[43m Phase: ***** install NPM ***** \033[0m"

curl -s https://deb.nodesource.com/setup_16.x | sudo bash
sudo apt update
sudo apt install nodejs -y

echo -e "\033[43m Phase: ***** Configure Game ***** \033[0m"
cd /srv/
git clone https://gitfront.io/r/deusops/JnacRhR4iD8q/2048-game.git
mv 2048-game/ 2048/
chown -R vagrant:vagrant 2048/
cd 2048/
npm install --include=dev
npm run build

echo -e "\033[43m Phase: ***** create 2048 service ***** \033[0m"
sudo cp -r /home/vagrant/provision/game/2048.service /etc/systemd/system/
sudo systemctl daemon-reload
sudo systemctl enable 2048.service
sudo systemctl start 2048.service