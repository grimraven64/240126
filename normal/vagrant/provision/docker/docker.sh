#!/bin/bash
echo -e "\033[43m Phase: ***** Install Curl ***** \033[0m"

sudo apt update && \
     apt install curl git

echo -e "\033[43m Phase: ***** Install Docker ***** \033[0m"

curl https://get.docker.com >> docker.sh
chmod +x docker.sh
sudo bash docker.sh

echo -e "\033[43m Phase: ***** pulling and run image ***** \033[0m"
docker pull registry.gitlab.com/grimraven64/240126:latest
docker run -dit -p 80:8080 --name 2048 registry.gitlab.com/grimraven64/240126:latest

